---
layout: handbook-page-toc
title: "Gainsight Maturity Scoring"
description: "An overview of Maturity Scoring, how it is calculated, and how TAMs can use the information with customers in their conversations."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

*For an overview of Gainsight, and information about how to login, please refer to the [Gainsight Overview Page](/handbook/sales/gainsight/).*

For an overview of how TAMs use Gainsight, please refer to the [Gainsight TAM Overview Page](/handbook/customer-success/tam/gainsight)

## Maturity Scoring

Use case maturity scoring will assist TAMs in understanding a customer's adoption state based on a specific list of metrics. 
By looking at the maturity scores, the TAM will gain an understanding of the customer's current state in the adoption journey.


### CI Maturity Scoring

The following primary and ancillary metrics are used to determine a customer's CI Maturity Score: 
<br>
![CI Maturity Scoring](https://lucid.app/publicSegments/view/14463ed0-bdf2-47a1-998b-40a6bdba9986/image.png)
<br>
[Adoption Guide Reference Link](/handbook/marketing/strategic-marketing/usecase-gtm/ci/#adoption-guide)

#### Resources

- [Use Case Maturity Scoring - Defined Metrics](https://docs.google.com/spreadsheets/d/1dJLQIwoQxSK6pJL-ZmbMK_VUBmY0INZPgVsWqsypHzI/edit?usp=sharing) (internal only)
- [Template Deck for Customer Conversations](https://docs.google.com/presentation/d/1Zn5gyUrBRgA1fyprVuoA24FKiH_3fpT5KuL5vK6GcuE/edit#slide=id.g110af81e0a3_0_215) (internal only)
    - In order to generate the slides shown in the video above - this [Google sheet](https://docs.google.com/spreadsheets/d/1wPrQRS9XGJek4oWcZPe9QeaFne9scbJVZYuvEioE2GI/edit#gid=1737266116) can help. 
        - Instructions on using the sheet can be found in this [internal video](https://youtu.be/oWuX_jtLnLI)
- Use the [Use Case Maturity Scorecard](https://gitlab.gainsightcloud.com/v1/ui/dashboard#/f18df482-a70f-4a8b-8c82-1f99538d777e) dashboard in Gainsight to view your customer's CI Metrics to export and calculate the Maturity score
- [Value Statements for CI Maturity Score](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/delivery-automation/#customer-adoption-and-value)
    - This should help guide the conversation around how CI maturity is measured and how to improve adoption.

#### Additional TAM Enablement 
In this (internal) video a fellow TAM walks you through:
- How the CI maturity score is calculated 
- Template slides to showcase a customer's maturity
- Key conversation drivers such as:
  - How the customer is currently progressing on their path to CI maturity
  - How the customer compares with other similar customer's in the industry 
  - Key areas of focus on the adoption journey

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/zurUFQDSWt8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>  
<!-- blank line -->

- CI Maturity Scoring Walk-thru 
    - [Session 1](https://youtu.be/E4IMgFWGkNM) (internal only)
    - [Session 2](https://chorus.ai/meeting/E4F00AFC0C4A4036A7AC370653A50112?) (internal only)
    - [Value Statements for CI Maturity](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/delivery-automation/#customer-adoption-and-value)


### DevSecOps Maturity Scoring
The following metrics are used to determine a customer's DevSecOps Maturity Score:
<br>
![DevSecOps Maturity Scoring](https://lucid.app/publicSegments/view/ab7a9ef5-d69c-47ff-9dc0-defe3e8f8610/image.png)
<br>
[Adoption Guide Reference Link](/marketing/strategic-marketing/usecase-gtm/devsecops/#adoption-guide)
<br>

#### Resources

- [Use Case Maturity Scoring - Defined Metrics](https://docs.google.com/spreadsheets/d/1dJLQIwoQxSK6pJL-ZmbMK_VUBmY0INZPgVsWqsypHzI/edit?usp=sharing) (internal only)
- [Template Deck for Customer Conversations](https://docs.google.com/presentation/d/1JdRlS5G9iB0XMMWktYxYBDCnhVs2Ti-I6jBW0q61YZk/edit?usp=sharing) (internal only)
- Use the [Use Case Maturity Scorecard](https://gitlab.gainsightcloud.com/v1/ui/dashboard#/f18df482-a70f-4a8b-8c82-1f99538d777e) dashboard in Gainsight to view your customer's DevSecOps Metrics to export and calculate the Maturity score
- Value Statements - Coming Soon
- Additional TAM Enablement - Coming Soon

##### **Disclaimer:**  The scoring criterias & thresholds will continue to evolve as we iterate
